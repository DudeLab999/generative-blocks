"use strict";

const path = require("path");
const isLocal = typeof process.pkg === "undefined";
const basePath = isLocal ? process.cwd() : path.dirname(process.execPath);
const fs = require("fs");
const { debug } = require("console");
const { resolve } = require("path");
const sha1 = require(path.join(basePath, "/node_modules/sha1"));
const { createCanvas, loadImage } = require(path.join(
    basePath,
    "/node_modules/canvas"
));
const buildDir = path.join(basePath, "/build");
const layersDir = path.join(basePath, "/layers");
console.log(path.join(basePath, "/src/config.js"));
const {
    format,
    baseUri,
    description,
    background,
    uniqueDnaTorrance,
    layerConfigurations,
    rarityDelimiter,
    shuffleLayerConfigurations,
    debugLogs,
    extraMetadata,
    specialLayers,
} = require(path.join(basePath, "/src/config.js"));
const canvas = createCanvas(format.width, format.height);
const ctx = canvas.getContext("2d");
var metadataList = [];
var attributesList = [];
var dnaList = [];
var specialLayer = [];
var globalPromises = [];

// Lid mean Full plate
const SPECIAL = 0;
const SURROUNDING = 1;
const OUT_OF_THE_BOX = 2;
const RIGHT_LID = 3;
const TOP_LID = 4;
const FRONT_LID = 5;
const TEXTURE = 0;
const LEFT_LID = 6;
const BACK_LID = 7;
const BOT_LID = 8;
const CENTER = 10;
const LEFT_TOP = 11;
const LEFT_BOT = 12;
const BACK_TOP = 13;
const BACK_BOT = 14;
const BOT_LEFT = 15;
const BOT_RIGHT = 16;

const specialList = [
    "Special",
    "Surrounding",
    "Out of the box",
    "Right Pattern",
    "Top Pattern",
    "Front Pattern",
    "Texture",
    "Left Pattern",
    "Back Pattern",
    "Bottom Pattern",

    "Center",
    "Left Top Pattern",
    "Left Botom Pattern",
    "Back Top Pattern",
    "Back Bottom Pattern",
    "Bottom Left Pattern",
    "Bottom Right Pattern"
    // "special",
    // "surrounding",
    // "out_of_the_box",
    // "right_lid",
    // "top_lid",
    // "front_lid",
    // "texture",
    // "left_lid",
    // "back_lid",
    // "bot_lid",
    // "center",
    // "left_top",
    // "left_bot",
    // "back_top",
    // "back_bot",
    // "bot_left",
    // "bot_right",
];

const buildSetup = () => {
    if (fs.existsSync(buildDir)) {
        fs.rmdirSync(buildDir, { recursive: true });
    }
    fs.mkdirSync(buildDir);
    fs.mkdirSync(path.join(buildDir, "/json"));
    fs.mkdirSync(path.join(buildDir, "/images"));
};

const getRarityWeight = (_str) => {
    let nameWithoutExtension = _str.slice(0, -4);
    var nameWithoutWeight = Number(
        nameWithoutExtension.split(rarityDelimiter).pop()
    );
    if (isNaN(nameWithoutWeight)) {
        nameWithoutWeight = 0;
    }
    return nameWithoutWeight;
};

const cleanDna = (_str) => {
    // console.log("-------")
    // console.log(_str);
    var dna = Number(_str.split(":").shift());
    return dna;
};

const cleanName = (_str) => {
    let nameWithoutExtension = _str.slice(0, -4);
    var nameWithoutWeight = nameWithoutExtension.split(rarityDelimiter).shift();
    return nameWithoutWeight;
};

const getElements = (path) => {
    return fs
        .readdirSync(path)
        .filter((item) => !/(^|\/)\.[^\/\.]/g.test(item))
        .map((i, index) => {
            return {
                id: index,
                name: cleanName(i),
                filename: i,
                path: `${path}${i}`,
                weight: getRarityWeight(i),
            };
        });
};

const layersSetup = (layersOrder) => {
    const layers = layersOrder.map((layerObj, index) => ({
        id: index,
        name: layerObj.name,
        elements: getElements(`${layersDir}/${layerObj.name}/`),
        blendMode:
            layerObj["blend"] != undefined ? layerObj["blend"] : "source-over",
        opacity: layerObj["opacity"] != undefined ? layerObj["opacity"] : 1,
    }));
    return layers;
};

const saveImage = (_editionCount) => {
    fs.writeFileSync(
        `${buildDir}/images/${_editionCount}.png`,
        canvas.toBuffer("image/png")
    );
};

const genColor = () => {
    let hue = Math.floor(Math.random() * 360);
    let pastel = `hsl(${hue}, 100%, ${background.brightness})`;
    return pastel;
};

const drawBackground = () => {
    ctx.fillStyle = genColor();
    ctx.fillRect(0, 0, format.width, format.height);
};

const addMetadata = (_dna, _edition) => {
    // console.log("---DNA---");
    // console.log(_dna)
    let dateTime = Date.now();
    let tempMetadata = {
        block_hash: sha1(_dna.join("")),
        name: `#${_edition}`,
        description: description,
        image: `${baseUri}/${_edition}.png`,
        edition: _edition,
        date: dateTime,
        ...extraMetadata,
        attributes: attributesList,
        creator: "Beedle NFT",
    };
    metadataList.push(tempMetadata);
    attributesList = [];
};

const addAttributes = (_element) => {
    // console.log("Attribute")

    let selectedElement = _element.layer.selectedElement;
    // debugLogs ? console.log("Add attr") : null;
    // debugLogs ? console.log(selectedElement) : null;
    attributesList.push({
        trait_type: _element.layer.name,
        value: selectedElement.name,
    });
};

const loadLayerImg = async (_layer) => {
    return new Promise(async (resolve) => {
        const image = await loadImage(`${_layer.selectedElement.path}`);
        resolve({ layer: _layer, loadedImage: image });
    });
};

const drawElement = (_renderObject) => {
    // debugLogs ? console.log(_renderObject): null;
    ctx.globalAlpha = _renderObject.layer.opacity;
    ctx.globalCompositeOperation = _renderObject.layer.blendMode;
    ctx.drawImage(_renderObject.loadedImage, 0, 0, format.width, format.height);
    addAttributes(_renderObject);
};

const constructLayerToDna = (_dna = [], _layers = []) => {
    // console.log(_layers);
    let mappedDnaToLayers = _layers.map((layer, index) => {
        let selectedElement = layer.elements.find(
            (e) => e.id == cleanDna(_dna[index])
        );
        return {
            name: layer.name,
            blendMode: layer.blendMode,
            opacity: layer.opacity,
            selectedElement: selectedElement,
        };
    });
    return mappedDnaToLayers;
};

const isDnaUnique = (_DnaList = [], _dna = []) => {
    let foundDna = _DnaList.find((i) => i.join("") === _dna.join(""));
    return foundDna == undefined ? true : false;
};

const createDna = (_layers) => {
    // loop over available layers
    let randNum = [];
    _layers.forEach((layer) => {
        var totalWeight = 0;
        layer.elements.forEach((element) => {
            totalWeight += element.weight;
        });
        // number between 0 - totalWeight
        let random = Math.floor(Math.random() * totalWeight);
        for (var i = 0; i < layer.elements.length; i++) {
            // subtract the current weight from the random weight until we reach a sub zero value.
            random -= layer.elements[i].weight;
            if (random < 0) {
                return randNum.push(
                    `${layer.elements[i].id}:${layer.elements[i].filename}`
                );
            }
        }
    });
    return randNum;
};

const getTransLayer = (layerTransparent) => {
    let transList = [];
    Object.entries(layerTransparent).forEach((el) => {
        if (el[1] == true) {
            transList.push(el[0]);
        }
        // console.log(el)
    })
    return transList;
}

const writeMetaData = (_data) => {
    fs.writeFileSync(`${buildDir}/json/_metadata.json`, _data);
};

const saveMetaDataSingleFile = (_editionCount) => {
    let metadata = metadataList.find((meta) => meta.edition == _editionCount);
    debugLogs
        ? console.log(
            `Writing metadata for ${_editionCount}: ${JSON.stringify(metadata)}`
        )
        : null;
    fs.writeFileSync(
        `${buildDir}/json/${_editionCount}.json`,
        JSON.stringify(metadata, null, 2)
    );
};

function shuffle(array) {
    let currentIndex = array.length,
        randomIndex;
    while (currentIndex != 0) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;
        [array[currentIndex], array[randomIndex]] = [
            array[randomIndex],
            array[currentIndex],
        ];
    }
    return array;
}

const getPropertyName = (name) => {
    let obj = {
        "background": "Background",
        "shadow": "Shadow",
        "bot": "Bottom Lid",
        "left": "Left Lid",
        "front": "Front Lid",
        "back": "Back Lid",
        "top": "Top lid",
        "right": "Right lid",
        "special": "Special",
        "surrounding": "Surrounding",
        "oob": "Out of the box",
        "right_lid": "Right Pattern",
        "top_lid": "Top Pattern",
        "front_lid": "Front Pattern",
        "texture": "Texture",
        "left_lid": "Left Pattern",
        "back_lid": "Back Pattern",
        "bot_lid": "Bottom Pattern",

        "center": "Center",
        "left_top": "Left Top Pattern",
        "left_bot": "Left Botom Pattern",
        "back_top": "Back Top Pattern",
        "back_bot": "Back Bottom Pattern",
        "bot_left": "Bottom Left Pattern",
        "bot_right": "Bottom Right Pattern"
    }
    return obj[name];
}

const matchLayer = (_layers = [], _layerTochecks = []) => {
    let countMatch = 0;
    for (let layer of _layers) {
        _layerTochecks.map((nameCheck) => {
            if (layer == nameCheck) {
                countMatch += 1;
            }
        })
    }
    return countMatch == _layerTochecks.length;
}

const drawOrderedLayer = async (_renderObjectArray, orderList, flag) => {
    let tmpList = [];
    let spIndex = -1;
    // console.log(orderList)
    let specialLoadingList = [];
    debugLogs ? console.log("OrderList: " + orderList) : null;
    orderList.forEach((layerName) => {
        layerName = getPropertyName(layerName);
        console.log(layerName);
        spIndex = specialList.indexOf(layerName);

        let res = _renderObjectArray.map((renderObject) => {
            // console.log("---------------");
            // console.log(renderObject)
            if (renderObject.layer.name == layerName) {
                return drawElement(renderObject);
            }
        });
        // console.log("---------------");
        // console.log(res);
        // If special layer is last order do this
        // Need test center layer or smthing
        if (spIndex >= 0) {
            debugLogs ? console.log("match spIndex: " + layerName) : null;
            generateSpecialLayerAndDraw([specialLayer[spIndex]], flag);
        }
    })


}

const createCustomLayer = async (countTransparent, loadedElements, layerTransparent) => {
    // หลักการคือ สร้าง Special layers ให้เสร็จก่อน
    // จากนั้นจึงค่อย Re-order layer ใหม่
    // orderLayer, specialLayerList ต้องสัมพันธ์กัน

    // console.log(countTransparent + " : countTransparent")
    if (countTransparent > 0) { countTransparent + " : countTransparent"; }


    let specialLayerList = [];
    let orderLayer = ["background", "shadow", "bot", "left", "front", "back", "top", "right"];

    debugLogs ? console.log("Creating Custom Layer...") : null;

    // debugLogs ? countTransparent = 1 : null;

    if (countTransparent >= 6) {
        orderLayer = ["background", "shadow", "bot", "left", "front", "back", "top", "right", "special"];

        specialLayerList = ["special"];

        // Prepare special layers
        getLoadedSpecialElement(specialLayerList, loadedElements);
    }
    else if (countTransparent == 0) {

        orderLayer = ["background", "shadow", "bot", "left", "left_lid", "bot_lid", "front", "front_lid", "back", "back_lid", "top", "top_lid", "right", "right_lid"];

        specialLayerList = ["left_lid", "bot_lid", "front_lid", "back_lid", "top_lid", "right_lid"];

        // if (Math.floor(Math.random() * 10) >= 7) {
        //     orderLayer = ["background", "shadow", "bot", "left", "front", "back", "top", "right", "texture", "surrounding"];
        //     specialLayerList = ["texture", "surrounding"];
        // }

        // Prepare special layers
        getLoadedSpecialElement(specialLayerList, loadedElements);
    }
    else if (countTransparent == 1) {
        // Check transparent part
        let transList = [];
        transList = getTransLayer(layerTransparent);
        // console.log("--------TransList-------");
        // console.log(transList);
        // console.log("--------TransList-------");
        if (transList.length >= 1) {
            let layerName = transList[0];

            orderLayer = ["background", "shadow", "bot", "left", "front", "back", "top", "right"];
            // orderLayer = ["background", "shadow", "bot", "left", "left_lid", "front", "front_lid","bot_lid", "back", "back_lid", "top", "top_lid", "right", "right_lid"];

            // specialLayerList = ["left_lid", "front_lid", "bot_lid", "back_lid", "top_lid", "right_lid"];

            if (layerName == "bot" || layerName == "left" || layerName == "back") {
                // Random surround the box or do nothing
                orderLayer = ["background", "shadow", "bot", "left", "front", "front_lid", "back", "top", "top_lid", "right", "right_lid"];
                specialLayerList = ["front_lid", "top_lid", "right_lid"];


                // if (Math.floor(Math.random() * 10) >= 8) {
                //     orderLayer.push("surrounding");
                //     specialLayerList.push("surrounding");
                // }
                // if (Math.floor(Math.random() * 10) >= 7) {
                //     // Add special prop surround the box => rare
                //     orderLayer.push("oob");
                //     specialLayerList.push("oob");
                // }
                // if (Math.floor(Math.random() * 10) >= 2) {
                //     orderLayer.push("texture");
                //     specialLayerList.push("texture");
                // }
            }
            else if (layerName == "front") {
                // orderLayer = ["background", "shadow", "bot", "bot_left", "left", "left_bot", "front", "back", "center", "top", "top_lid", "right", "right_lid"];
                orderLayer = ["background", "shadow", "bot", "left", "front", "back", "top", "top_lid", "right", "right_lid"];
                specialLayerList = ["left_lid", "bot_lid", "back_lid", "top_lid", "right_lid"];

                // if (Math.floor(Math.random() * 10) >= 9) {
                //     orderLayer.push("oob");
                //     specialLayerList.push("oob");
                // }
            }
            else if (layerName == "top") {
                // Add random rate
                // orderLayer = ["background", "shadow", "bot", "left", "front", "front_lid", "back", "back_top", "top", "right", "left_top"];
                // specialLayerList = ["front_lid", "back_top", "left_top"];


                orderLayer = ["background", "shadow", "bot", "bot_lid", "left", "left_lid", "front", "front_lid", "back", "back_lid", "top", "right", "right_lid"];
                specialLayerList = ["bot_lid", "left_lid", "back_lid", "right_lid"];



                // if (Math.floor(Math.random() * 10) >= 9) {
                //     orderLayer.push("oob");
                //     specialLayerList.push("oob");
                // }
            }
            else if (layerName == "right") {
                // orderLayer = ["background", "shadow", "bot", "bot_right", "left", "front", "front_lid", "back", "back_bot", "top", "top_lid", "right"];
                // specialLayerList = ["bot_right", "front_lid", "back_bot", "top_lid"];

                orderLayer = ["background", "shadow", "bot", "bot_lid", "left", "left_lid", "front", "front_lid", "back", "top", "top_lid", "right"];
                specialLayerList = ["bot_lid", "left_lid", "front_lid", "back_lid", "top_lid"];

                // if (Math.floor(Math.random() * 10) >= 9) {
                //     orderLayer.push("oob");
                //     specialLayerList.push("oob");
                // }
            }

            getLoadedSpecialElement(specialLayerList, loadedElements);
        } else {
            // orderLayer = ["background", "shadow", "bot", "left", "front", "back", "top", "right", "texture"];

            // specialLayerList = ["texture"];

            // if (Math.floor(Math.random() * 10) >= 7) {
            //     orderLayer = ["background", "shadow", "bot", "left", "front", "back", "top", "right", "texture", "surrounding"];
            //     specialLayerList = ["texture", "surrounding"];

            // }

            orderLayer = ["background", "shadow", "bot", "left", "front", "back", "top", "top_lid", "right", "right_lid"];
            specialLayerList = ["left_lid", "bot_lid", "back_lid", "top_lid", "right_lid"];

        }
    }
    else if (countTransparent == 2) {
        // Check transparent part
        let transList = [];
        transList = getTransLayer(layerTransparent);
        // Must >= 2 to activate
        if (transList.length >= 2) {
            let layerName1 = transList[0];
            let layerName2 = transList[1];

            if ((layerName1 == "front" || layerName1 == "right") && (layerName2 == "front" || layerName2 == "right")) {
                // center box visible
                // Gen center interior | rare whole image
                orderLayer = ["background", "shadow", "bot", "left", "left_bot", "front", "back", "back_bot", "center", "top", "top_lid", "right"];

                specialLayerList = ["left_bot", "back_bot", "center", "top_lid",];

                if (Math.floor(Math.random() * 10) >= 8) {
                    orderLayer.push("oob");
                    specialLayerList.push("oob");
                }
            }

            else if ((layerName1 == "bot" || layerName1 == "right") && (layerName2 == "bot" || layerName2 == "right")) {
                // center box visible
                // Gen right interior | rare whole image
                orderLayer = ["background", "shadow", "bot", "left", "front", "front_lid", "back", "back_lid", "top", "top_lid", "right", "bot_right"];

                specialLayerList = ["front_lid", "back_lid", "top_lid", "bot_right",];


                if (Math.floor(Math.random() * 10) >= 9) {
                    orderLayer = ["background", "shadow", "bot", "left", "center", "front", "front_lid", "back", "back_lid", "top", "top_lid", "right", "bot_right"];
                    specialLayerList = ["center", "front_lid", "back_lid", "top_lid", "bot_right"];
                }
                if (Math.floor(Math.random() * 10) >= 8) {
                    orderLayer.push("oob");
                    specialLayerList.push("oob");
                }
            }

            else if ((layerName1 == "bot" || layerName1 == "top") && (layerName2 == "bot" || layerName2 == "top")) {
                // Top center visible
                orderLayer = ["background", "shadow", "bot", "left", "front", "front_lid", "back", "top", "right", "right_lid", "left_top", "back_top"];

                specialLayerList = ["front_lid", "right_lid", "left_top", "back_top",];

                if (Math.floor(Math.random() * 10) >= 8) {
                    orderLayer.push("oob");
                    specialLayerList.push("oob");
                }
            }

            else if ((layerName1 == "bot" || layerName1 == "front") && (layerName2 == "bot" || layerName2 == "front")) {
                // left center visible
                orderLayer = ["background", "shadow", "bot", "left", "left_bot", "front", "back", "top", "top_lid", "right", "right_lid", "bot_left"];

                specialLayerList = ["left_bot", "top_lid", "right_lid", "bot_left",];

                if (Math.floor(Math.random() * 10) >= 8) {
                    orderLayer.push("oob");
                    specialLayerList.push("oob");
                }
            }

            else if ((layerName1 == "bot" || layerName1 == "left") && (layerName2 == "bot" || layerName2 == "left") ||
                (layerName1 == "bot" || layerName1 == "back") && (layerName2 == "bot" || layerName2 == "back")) {
                // Normal draw
                orderLayer = ["background", "shadow", "bot", "left", "front", "back", "top", "right", "texture"];

                specialLayerList = ["texture"];

                if (Math.floor(Math.random() * 10) >= 8) {
                    orderLayer.push("oob");
                    specialLayerList.push("oob");
                }
            }
        }
    }
    else if (countTransparent == 3) {
        // Check transparent part
        let transList = [];
        transList = getTransLayer(layerTransparent);
        // Must >= 2 to activate
        if (transList.length >= 3) {
            let layerName1 = transList[0];
            let layerName2 = transList[1];
            let layerName3 = transList[2];

            if ((layerName1 == "bot" || layerName1 == "back" || layerName1 == "top") && (layerName2 == "bot" || layerName2 == "back" || layerName2 == "top") &&
                (layerName3 == "bot" || layerName3 == "back" || layerName3 == "top")) {

                orderLayer = ["background", "shadow", "bot", "left", "front", "front_lid", "back", "back_top", "top", "top_lid", "right", "right_lid"];

                specialLayerList = ["front_lid", "back_top", "top_lid", "right_lid"];

                if (Math.floor(Math.random() * 10) >= 8) {
                    orderLayer.push("surrounding");
                    specialLayerList.push("surrounding");
                }

            }
            else if ((layerName1 == "bot" || layerName1 == "back" || layerName1 == "right") && (layerName2 == "bot" || layerName2 == "back" || layerName2 == "right") &&
                (layerName3 == "bot" || layerName3 == "back" || layerName3 == "right")) {

                orderLayer = ["background", "shadow", "bot", "bot_right", "left", "front", "front_lid", "back", "top", "top_lid", "right"];

                specialLayerList = ["bot_right", "front_lid", "top_lid"];

                if (Math.floor(Math.random() * 10) >= 8) {
                    orderLayer.push("surrounding");
                    specialLayerList.push("surrounding");
                }

            }
            else if ((layerName1 == "bot" || layerName1 == "back" || layerName1 == "front") && (layerName2 == "bot" || layerName2 == "back" || layerName2 == "front") &&
                (layerName3 == "bot" || layerName3 == "back" || layerName3 == "front")) {

                orderLayer = ["background", "shadow", "bot", "bot_left", "left", "left_bot", "front", "back", "top", "top_lid", "right", "right_lid"];

                specialLayerList = ["bot_left", "left_bot", "top_lid", "right_lid"];

                if (Math.floor(Math.random() * 10) >= 8) {
                    orderLayer.push("surrounding");
                    specialLayerList.push("surrounding");
                }

            }
            else if ((layerName1 == "bot" || layerName1 == "back" || layerName1 == "left") && (layerName2 == "bot" || layerName2 == "back" || layerName2 == "left") &&
                (layerName3 == "bot" || layerName3 == "back" || layerName3 == "left")) {

                orderLayer = ["background", "shadow", "bot", "left", "front", "back", "top", "right", "texture"];

                specialLayerList = ["texture"];

            }
            else if ((layerName1 == "bot" || layerName1 == "top" || layerName1 == "left") && (layerName2 == "bot" || layerName2 == "top" || layerName2 == "left") &&
                (layerName3 == "bot" || layerName3 == "top" || layerName3 == "left")) {

                orderLayer = ["background", "shadow", "bot", "left", "left_top", "front", "front_lid", "back", "back_top", "top", "right"];

                specialLayerList = ["left_top", "front_lid", "back_top"];

            }
            else if ((layerName1 == "bot" || layerName1 == "top" || layerName1 == "right") && (layerName2 == "bot" || layerName2 == "top" || layerName2 == "right") &&
                (layerName3 == "bot" || layerName3 == "top" || layerName3 == "right")) {
                // order : bot left front back [props center] top right

                if (Math.floor(Math.random() * 10) >= 5) {
                    orderLayer = ["background", "shadow", "bot", "left", "left_top", "front", "front_lid", "back", "back_top", "center", "top", "right"];
                    specialLayerList = ["left_top", "front_lid", "back_top", "center"];
                } else {
                    orderLayer = ["background", "shadow", "bot", "bot_right", "left", "left_top", "front", "back", "center", "top", "right"];
                    specialLayerList = ["left_top", "front_lid", "back_top", "center"];
                }

                if (Math.floor(Math.random() * 10) >= 8) {
                    // Add special prop surround the box => rare | out of the box
                    orderLayer.push("surrounding");
                    specialLayerList.push("surrounding");
                }
            }
            else if ((layerName1 == "top" || layerName1 == "right" || layerName1 == "back") && (layerName2 == "top" || layerName2 == "right" || layerName2 == "back") &&
                (layerName3 == "top" || layerName3 == "right" || layerName3 == "back")) {
                // order : bot left front back [props center] top right

                if (Math.floor(Math.random() * 10) >= 4) {
                    orderLayer = ["background", "shadow", "bot", "bot_right", "left", "left_top", "front", "front_lid", "back", "center", "top", "right"];
                    specialLayerList = ["bot_right", "left_top", "front_lid", "center"];
                } else {
                    orderLayer = ["background", "shadow", "bot", "bot_right", "left", "left_top", "front", "back", "center", "top", "right"];
                    specialLayerList = ["bot_right", "left_top", "center"];
                }

                if (Math.floor(Math.random() * 10) >= 8) {
                    // Add special prop surround the box => rare | out of the box
                    orderLayer.push("surrounding");
                    specialLayerList.push("surrounding");
                }

            }

            else if ((layerName1 == "top" || layerName1 == "right" || layerName1 == "front") && (layerName2 == "top" || layerName2 == "right" || layerName2 == "front") &&
                (layerName3 == "top" || layerName3 == "right" || layerName3 == "front")) {

                // order : bot left front back [props center] top right
                if (Math.floor(Math.random() * 10) >= 5) {
                    drawOrderedLayer(renderObject, ["background", "shadow", "bot", "left", "left_lid", "front", "back", "back_lid", "center", "top", "right"], 0);
                } else {
                    drawOrderedLayer(renderObject, ["background", "shadow", "bot", "left", "left_top", "front", "back", "back_lid", "center", "top", "right"], 0);
                }

                if (Math.floor(Math.random() * 10) >= 5) {
                    orderLayer = ["background", "shadow", "bot", "left", "left_lid", "front", "back", "back_lid", "center", "top", "right"];
                    specialLayerList = ["left_lid", "back_lid", "center"];
                } else {
                    orderLayer = ["background", "shadow", "bot", "left", "left_top", "front", "back", "back_lid", "center", "top", "right"];
                    specialLayerList = ["bot_right", "left_top", "center"];
                }

                if (Math.floor(Math.random() * 10) >= 8) {
                    // Add special prop surround the box => rare | out of the box
                    orderLayer.push("surrounding");
                    specialLayerList.push("surrounding");
                }
            }

            else if ((layerName1 == "top" || layerName1 == "front" || layerName1 == "back") && (layerName2 == "top" || layerName2 == "front" || layerName2 == "back") &&
                (layerName3 == "top" || layerName3 == "front" || layerName3 == "back")) {
                // order : bot left front back [props center] top right

                // Draw with other style
                if (Math.floor(Math.random() * 10) >= 5) {
                    orderLayer = ["background", "shadow", "bot", "bot_lid", "left", "left_lid", "front", "back", "center", "top", "right", "right_lid"];
                    specialLayerList = ["bot_lid", "left_lid", "center", "right_lid"];

                } else if (Math.floor(Math.random() * 10) >= 7) {
                    orderLayer = ["background", "shadow", "bot", "bot_lid", "left", "left_top", "front", "back", "center", "top", "right", "right_lid"];
                    specialLayerList = ["bot_lid", "left_top", "center", "right_lid"];
                } else {
                    orderLayer = ["background", "shadow", "bot", "bot_left", "left", "left_bot", "front", "back", "center", "top", "right", "right_lid"];
                    specialLayerList = ["bot_left", "left_bot", "center", "right_lid"];
                }
                if (Math.floor(Math.random() * 10) >= 8) {
                    // Add special prop surround the box => rare | out of the box
                    orderLayer.push("surrounding");
                    specialLayerList.push("surrounding");
                }
            }
        }
    }
    else if (countTransparent == 4) {
        let transList = [];
        transList = getTransLayer(layerTransparent);
        // Must >= 2 to activate
        if (transList.length >= 4) {
            let layerName1 = transList[0];
            let layerName2 = transList[1];
            let layerName3 = transList[2];
            let layerName4 = transList[3];
            if (matchLayer(["bot", "back", "top", "right"], [layerName1, layerName2, layerName3, layerName4])) {
                // Left triangle

                // Check if exclude shadow pass?
                if (Math.floor(Math.random() * 10) >= 5) {
                    orderLayer = ["background", "bot", "left", "left_lid", "front", "back", "center", "top", "right", "oob"];
                    specialLayerList = ["left_lid", "center", "oob"];

                } else {
                    orderLayer = ["background", "bot", "left", "left_top", "front", "front_lid", "back", "center", "top", "right", "surrounding"];
                    specialLayerList = ["left_top", "front_lid", "center", "surrounding"];
                }
            }
            else if (matchLayer(["bot", "back", "top", "left"], [layerName1, layerName2, layerName3, layerName4])) {
                // Left triangle

                // Check if exclude shadow pass?
                if (Math.floor(Math.random() * 10) >= 5) {
                    orderLayer = ["background", "bot", "left", "front", "front_lid", "back", "top", "right", "right_lid", "oob"];
                    specialLayerList = ["front_lid", "right_lid", "oob"];

                } else {
                    orderLayer = ["background", "bot", "left", "front", "front_lid", "back", "top", "right", "right_lid", "oob", "surrounding"];
                    specialLayerList = ["front_lid", "right_lid", "oob", "surrounding"];
                }
            }
            else if (matchLayer(["bot", "back", "top", "left"], [layerName1, layerName2, layerName3, layerName4])) {
                // Left triangle

                // Check if exclude shadow pass?
                if (Math.floor(Math.random() * 10) >= 5) {
                    orderLayer = ["background", "bot", "left", "front", "front_lid", "back", "top", "right", "right_lid", "oob"];
                    specialLayerList = ["front_lid", "right_lid", "oob"];

                } else {
                    orderLayer = ["background", "bot", "left", "front", "front_lid", "back", "top", "right", "right_lid", "oob", "surrounding"];
                    specialLayerList = ["front_lid", "right_lid", "oob", "surrounding"];
                }
            }
            else if (matchLayer(["bot", "back", "top", "front"], [layerName1, layerName2, layerName3, layerName4])) {
                orderLayer = ["background", "shadow", "bot", "left", "left_lid", "center", "front", "back", "top", "right", "right_lid", "surrounding"];
                specialLayerList = ["left_lid", "right_lid", "surrounding"];
            }

            else if (matchLayer(["bot", "left", "back", "right"], [layerName1, layerName2, layerName3, layerName4])) {
                orderLayer = ["background", "shadow", "bot", "left", "center", "front", "front_lid", "back", "top", "top_lid", "right", "surrounding"];
                specialLayerList = ["center", "front_lid", "top_lid", "surrounding"];
            }

            else if (matchLayer(["bot", "left", "back", "front"], [layerName1, layerName2, layerName3, layerName4])) {
                orderLayer = ["background", "shadow", "bot", "left", "center", "front", "back", "top", "top_lid", "right", "right_lid", "surrounding"];
                specialLayerList = ["center", "front_lid", "top_lid", "surrounding"];
            }

            else if (matchLayer(["bot", "left", "back", "top"], [layerName1, layerName2, layerName3, layerName4])) {
                orderLayer = ["background", "shadow", "bot", "left", "center", "front", "back", "top", "top_lid", "right", "right_lid", "surrounding"];
                specialLayerList = ["center", "front_lid", "top_lid", "surrounding"];
            }
            // bot,back,top,right
            // bot,back,top,left
            // bot,back,top,front

            // ป้ายรอรถเมล์ ด้านหลัง
            // bot,left,back,right
            // "bot", "left", "back", "top"

            // ป้ายรอรถเมล์
            // bot, front, back, right

        }

    }

    // Orderable here
    debugLogs ? console.log("---- Reorder -----") : null;
    debugLogs ? console.log(orderLayer) : null;
    let spLayerLeft = specialLayerList.length;
    loadedElements.map((el, index) => {
        // Is Current index belongs to Special layer
        let _layerName = getPropertyName(orderLayer[index]);
        // console.log(_layerName);

        let _spIndex = specialList.indexOf(_layerName);
        if (_spIndex >= 0) {
            // get sp layer from current element
            // if 2 left | bg,shadow,...special,texture
            // loadedElements[loadedElements.length-1] // -1 = ตัวสุดท้าย
            if (spLayerLeft > 0) {
                let tempEleToShuffle = el;
                let indexofSpecialLayerInElements = loadedElements.length - (spLayerLeft);
                // console.log(indexofSpecialLayerInElements);
                let spLayerToShuffle = loadedElements[indexofSpecialLayerInElements];
                spLayerLeft -= 1;
                loadedElements.splice(indexofSpecialLayerInElements, 1);
                loadedElements.splice(index, 0, spLayerToShuffle);
            }
        }
    })
    return loadedElements;
}

const getLoadedSpecialElement = async (specialLayerList, loadedElements) => {
    // Prepare special layers
    let layerName = "";
    let loadedLayerImage = [];
    let spIndex = -1;

    for (let spLayer of specialLayerList) {
        layerName = getPropertyName(spLayer);

        spIndex = specialList.indexOf(layerName);

        if (spIndex >= 0) {
            debugLogs ? console.log("match spIndex: " + layerName) : null;
            loadedLayerImage = generateSpecialLayerAndDraw([specialLayer[spIndex]]);
            loadedElements.push(loadedLayerImage);
        }
    }
    return loadedElements;
}

const generateSpecialLayerAndDraw = async (_specialLayers, flag) => {
    // Random file
    let spDna = createDna(_specialLayers); // id:fileName ที่สุ่มแล้ว

    // 2 Built layer
    let results = constructLayerToDna(spDna, _specialLayers); // ได้ object ของ layer ที่สุ่ม

    let loadLayerImage = [];

    for (let _layer of results) {
        // console.log("_layer")
        // console.log(_layer)
        loadLayerImage = new Promise(async (res) => {
            const loadingImage = await loadImage(`${_layer.selectedElement.path}`)
            res({ layer: _layer, loadedImage: loadingImage });
        });
    }
    return loadLayerImage;
    // loadLayerImage.then((loadedLayerImage) => {
    //     console.log("PROM--------")
    //     console.log(loadedLayerImage)
    //     return loadedLayerImage;
    // });


    // _loadedElementsIn.push(loadLayerImage);

    // return _loadedElementsIn;

}

const startCreating = async () => {
    let layerConfigIndex = 0;
    let editionCount = 1;
    let failedCount = 0;
    let abstractedIndexes = [];

    for (let i = 1; i <= layerConfigurations[layerConfigurations.length - 1].growEditionSizeTo; i++) {
        abstractedIndexes.push(i);
        // console.log(i)
    }

    if (shuffleLayerConfigurations) {
        abstractedIndexes = shuffle(abstractedIndexes);
    }
    debugLogs
        ? console.log("Editions left to create: ", abstractedIndexes)
        : null;

    // for multiple layer config set [multiple edition]
    while (layerConfigIndex < layerConfigurations.length) {
        // Main layers order [bg,eye,lid]
        // Clear canvas before draw prevent ghosting

        ctx.clearRect(0, 0, format.width, format.height);
        const layers = layersSetup(
            layerConfigurations[layerConfigIndex].layersOrder
        );

        // Setup Special Layers ready to use
        specialLayer = layersSetup(specialLayers);

        while (editionCount <= layerConfigurations[layerConfigIndex].growEditionSizeTo) {
            let newDna = createDna(layers); // id:fileName
            // console.log(newDna)
            if (isDnaUnique(dnaList, newDna)) {
                let results = constructLayerToDna(newDna, layers);
                let loadedElements = [];

                let countTransparent = 0;

                let layerTransparent = {
                    bg: false,
                    shadow: false,
                    bot: false,
                    left: false,
                    front: false,
                    back: false,
                    top: false,
                    right: false,
                }

                const drawMainLayer = new Promise((resolve, reject) => {
                    for (let _layer of results) {
                        // console.log("Hello");
                        // console.log(_layer);

                        if (_layer.name != "Background" && _layer.name != "Shadow") {
                            let isTransparent = false;
                            if (_layer.selectedElement.filename.includes("trans") || _layer.selectedElement.filename.includes("Trans")) {
                                isTransparent = true;
                            }
                            let layerName = _layer.name;

                            // Set transparent true/false
                            if (isTransparent) {
                                countTransparent += 1;
                                layerTransparent[layerName] = true;
                            }
                        }

                        const loadLayerImage = new Promise(async (res) => {
                            const loadingImage = await loadImage(`${_layer.selectedElement.path}`)
                            res({ layer: _layer, loadedImage: loadingImage });
                        });

                        loadedElements.push(loadLayerImage);
                    };
                    // console.log(loadedElements)
                    resolve(loadedElements);

                })

                // let a2 = [];
                createCustomLayer(countTransparent, loadedElements, layerTransparent);

                // loadedElements.push(createCustomLayer(countTransparent, "special"));
                // console.log("------loadedElements-----");
                // console.log(loadedElements);

                // console.log("------a2-----");
                // console.log(a2);

                // console.log("------loadedElements 222-----");
                // console.log(loadedElements);

                await Promise.all(loadedElements).then(renderObjectArray => {
                    // console.log(rendered)
                    ctx.clearRect(0, 0, format.width, format.height);
                    renderObjectArray.map((renderObject) => {
                        // console.log(renderObject)
                        drawElement(renderObject);
                    });

                    saveImage(abstractedIndexes[0]);
                    addMetadata(newDna, abstractedIndexes[0]);
                    saveMetaDataSingleFile(abstractedIndexes[0]);
                    console.log(
                        `Created edition: ${abstractedIndexes[0]}, with DNA: ${sha1(
                            newDna.join("")
                        )}`
                    );
                });
                dnaList.push(newDna);
                editionCount++;
                abstractedIndexes.shift();

                // break;

                // End if dupplicated
            }
            else {
                console.log("Block exists!");
                failedCount++;
                if (failedCount >= uniqueDnaTorrance) {
                    console.log(
                        `You need more layers or elements to grow your edition to ${layerConfigurations[layerConfigIndex].growEditionSizeTo} artworks!`
                    );
                    process.exit();
                }
            }
        }
        layerConfigIndex++;
    }
    writeMetaData(JSON.stringify(metadataList, null, 2));
};

module.exports = { startCreating, buildSetup, getElements };
