"use strict";

const path = require("path");
const isLocal = typeof process.pkg === "undefined";
const basePath = isLocal ? process.cwd() : path.dirname(process.execPath);
const { MODE } = require(path.join(basePath, "src/blendMode.js"));
const description =
  "Welcome to the mysterios blocks, uniques 1000 NFT blocks with abundant packaging and items.";
const baseUri = "ipfs://NewUriToReplace";

const layerConfigurations = [
  {
    growEditionSizeTo: 50,
    layersOrder: [
      { name: "Background" },
      { name: "Shadow" },
      { name: "Bottom Lid" },
      { name: "Left Lid" },
      { name: "Front Lid" },
      { name: "Back Lid" },
      { name: "Top lid" },
      { name: "Right lid" }
    ],
  },
];

const shuffleLayerConfigurations = false;

const debugLogs = false;

const format = {
  width: 2480,
  height: 3507,
};

const background = {
  generate: false,
  brightness: "80%",
};

const extraMetadata = {
  creator: "Beedle NFT",
};

const rarityDelimiter = "#";

const uniqueDnaTorrance = 10000;

const preview = {
  thumbPerRow: 5,
  thumbWidth: 50,
  imageRatio: format.width / format.height,
  imageName: "preview.png",
};

const specialLayers = [
  { name: "Special" },
  { name: "Surrounding" },
  { name: "Out of the box" },
  { name: "Right Pattern" },
  { name: "Top Pattern" },
  { name: "Front Pattern" },
  { name: "Texture" },
  { name: "Left Pattern" },
  { name: "Back Pattern" },
  { name: "Bottom Pattern" },

  { name: "Center" },
  { name: "Left Top Pattern" },
  { name: "Left Bottom Pattern" },
  { name: "Back Top Pattern" },
  { name: "Back Bottom Pattern" },
  { name: "Bottom Left Pattern" },
  { name: "Bottom Right Pattern" },
];

module.exports = {
  format,
  baseUri,
  description,
  background,
  uniqueDnaTorrance,
  layerConfigurations,
  rarityDelimiter,
  preview,
  shuffleLayerConfigurations,
  debugLogs,
  extraMetadata,
  specialLayers,
};
